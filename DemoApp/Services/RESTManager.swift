//
//  RESTManager.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import Foundation

enum RESTType {
    case get
    case post (JSONDictionary?)
    case put  (JSONDictionary?)
    case delete
}

struct REST_API {
    // Core
    static let REST_URL = "https://randomuser.me"
    
    // User
    static func getUsers(_ page: Int, results: Int = 20) -> URL {
        return URL(string: "\(REST_URL)/api/?page=\(page)&results=\(results)")!
    }
}

typealias JSONDictionary = [String: Any]

struct Response_NONE: Decodable {}

struct RESTError: Error {
    let localizedDescription: String
    let statusCode: Int?
    
    init?(_ localizedDescription: String, statusCode: Int? = nil) {
        self.localizedDescription = localizedDescription
        self.statusCode = statusCode
    }
}

struct RESTService<T: Decodable> {
    
    let type: RESTType
    let url: URL
    let authorize: Bool
    let response: ((T?, RESTError?) -> Swift.Void)
    
    init(type: RESTType, url: URL, authorize: Bool, response: @escaping ((T?, RESTError?) -> Swift.Void)) {
        self.type = type
        self.url = url
        self.authorize = authorize
        self.response = response
    }
    
    func perform() {
        var urlRequest = URLRequest(url: url)
        
        // headers
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue("iOS", forHTTPHeaderField: "X-Client-Platform")
        
        // authorization
        if authorize {
            // load and set token
        }
        
        // body
        func setBody(_ body: JSONDictionary?) {
            if let body = body {
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body)
                } catch let error {
                    print("Can't serialize data: \(error.localizedDescription)")
                    response(nil, RESTError("Can't serialize data: \(error.localizedDescription)", statusCode: 666))
                    return
                }
            }
        }
        
        switch type {
        case .get:            urlRequest.httpMethod = "GET"
        case .delete:         urlRequest.httpMethod = "DELETE"
        case .post(let body): urlRequest.httpMethod = "POST";   setBody(body)
        case .put(let body):  urlRequest.httpMethod = "PUT";    setBody(body)
        }
        
        startSession(urlRequest: urlRequest)
    }
    
    private func startSession(urlRequest: URLRequest) {
        URLSession(configuration: .default).dataTask(with: urlRequest) { (data, response, error) in
            
            // This will catch "Request timed out."
            if error != nil {
                self.response(nil, RESTError(error!.localizedDescription, statusCode: 0))
                return
            }
            
            let statusCode = (response as? HTTPURLResponse)!.statusCode
            
            // Check for errors
            if statusCode > 300 {
                
                // Try to parse custom error message
                if let message = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? JSONDictionary {
                    if let messageStr = message["message"] as? String ?? message["error"] as? String {
                        self.response(nil, RESTError(messageStr, statusCode: statusCode))
                        return
                    }
                }
                
                // Return default error messages
                switch statusCode {
                case 400: self.response(nil, RESTError("Bad request.", statusCode: statusCode))
                case 403: self.response(nil, RESTError("Restricted access.", statusCode: statusCode))
                case 404: self.response(nil, RESTError("Data not found", statusCode: statusCode))
                case 422: self.response(nil, RESTError("Data error occurred, please try again later.", statusCode: statusCode))
                case 500: self.response(nil, RESTError("Internal server error, please try again later.", statusCode: statusCode))
                default:  self.response(nil, RESTError("Error occurred.", statusCode: statusCode))
                }
                
            } else {
                
                // Special case with no content
                if statusCode == 204 {
                    self.response(nil, nil)
                    return
                }
                
                // Parse content
                do {
                    if let data = data {
                        self.response(try JSONDecoder().decode(T.self, from: data), nil)
                    } else {
                        self.response(nil, RESTError("Data equal to nil", statusCode: statusCode))
                    }
                } catch let DecodingError.dataCorrupted(context) {
                    self.response(nil, RESTError(context.debugDescription, statusCode: statusCode))
                } catch let DecodingError.keyNotFound(_, context) {
                    self.response(nil, RESTError(context.debugDescription, statusCode: statusCode))
                } catch let DecodingError.typeMismatch(_, context) {
                    self.response(nil, RESTError(context.debugDescription, statusCode: statusCode))
                } catch let DecodingError.valueNotFound(_, context) {
                    self.response(nil, RESTError(context.debugDescription, statusCode: statusCode))
                } catch let error {
                    self.response(nil, RESTError(error.localizedDescription, statusCode: statusCode))
                }
                
            }
            
            }.resume()
    }
    
}
