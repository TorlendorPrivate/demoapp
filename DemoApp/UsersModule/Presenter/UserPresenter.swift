//
//  UserPresenter.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import Foundation

/// User Presenter Protocol
protocol UserPresenterProtocol : class {
    /// The Interactor will inform the Presenter a successful fetch.
    func interactor(_ interactor: UserInteractorProtocol, didFetch object: UserModelWrapper?)
    /// The Interactor will inform the Presenter a failed fetch.
    func interactor(_ interactor: UserInteractorProtocol, didFailWith error: RESTError)
}

class UserPresenter {
    weak var view: UserViewProtocol?
    var interactor: UserAPIManagerProtocol?
}

extension UserPresenter: UserPresenterProtocol {
    func interactor(_ interactor: UserInteractorProtocol, didFetch object: UserModelWrapper?) {
        if let users = object?.results {
             view?.set(users: users)
        } else {
            view?.showError(errorMsg: "Some error happend while loading user please try again later.")
        }
    }
    
    func interactor(_ interactor: UserInteractorProtocol, didFailWith error: RESTError) {
        view?.showError(errorMsg: error.localizedDescription)
    }
}
