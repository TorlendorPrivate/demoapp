//
//  UserInteractor.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import Foundation

/// User Module Interactor Protocol
protocol UserInteractorProtocol {
    // Fetch User Model from Data Layer
    func fetchUsers(forPage page: Int)
}

class UserInteractor: UserInteractorProtocol {
    private let apiManager: UserAPIManagerProtocol
    
    var presenter: UserPresenterProtocol?
    
    required init(withApiManager apiManager:UserAPIManagerProtocol) {
        self.apiManager = apiManager
    }
    
    func fetchUsers(forPage page: Int) {
        apiManager.fetchUsers(page: page) { [unowned self] (object, error) in
            if let error = error  {
                self.presenter?.interactor(self, didFailWith: error)
            } else {
                self.presenter?.interactor(self, didFetch: object)
            }
        }
    }
}
