//
//  UserAPIWorker.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import Foundation

protocol UserAPIManagerProtocol {
    func fetchUsers(page: Int, callBack: @escaping (UserModelWrapper?, RESTError?) -> Void)
}

class UserAPIManager: UserAPIManagerProtocol {
    
    func fetchUsers(page: Int, callBack: @escaping (UserModelWrapper?, RESTError?) -> Void) {
        if Reachability.isConnectedToNetwork {
            RESTService<UserModelWrapper>(type: .get, url: REST_API.getUsers(page), authorize: false, response: { data, error in
                callBack(data, error)
            }).perform()
        } else {
            callBack(nil, RESTError("No internet connection. Please make sure you are connected to the internet in order to complete this action."))
        }
    }
    
}
