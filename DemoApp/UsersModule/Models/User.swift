//
//  User.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import Foundation

struct UserModelWrapper: Decodable {
    let results: [User]
}

struct User: Decodable {
    let gender: String
    let name: Name
    let email: String
    let dob: DOB
    let picture: Picture
    let nat: String
}

struct Name: Decodable {
    let title: String
    let first: String
    let last: String
}

struct DOB: Decodable {
    let date: String
    let age: Int
}

struct Picture: Decodable {
    let large: String
    let medium: String
    let thumbnail: String
}
