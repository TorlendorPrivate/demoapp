//
//  UserTableViewCell.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import UIKit
import Kingfisher

class UserTableViewCell: UITableViewCell {
    
    static let cid: String = "UserCell"

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var nationalityImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateUI(user: User) {
        
        userImageView.kf.setImage(with: URL(string: user.picture.medium))
        nameLabel.text = "\(user.name.title) \(user.name.first) \(user.name.last)"
        ageLabel.text = "Age: \(user.dob.age)"
        nationalityImageView.kf.setImage(with: URL(string: "https://www.countryflags.io/\(user.nat)/flat/64.png"))
    }

}
