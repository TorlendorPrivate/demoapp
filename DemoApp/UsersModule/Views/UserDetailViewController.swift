//
//  UserDetailViewController.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    /// Class storyboard identifier.
    class var identifier: String { return "UserDetailViewController" }
    
    /// Init method that will init and return view controller.
    class func instantiate(withUser user: User) -> UserDetailViewController {
        let viewController = UIStoryboard.main.instantiate(identifier) as! UserDetailViewController
        viewController.user = user
        
        return viewController
    }
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var emailTextView: UITextView!
    
    private var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        userImageView.kf.setImage(with: URL(string: user.picture.large))
        nameLabel.text = "\(user.name.title) \(user.name.first) \(user.name.last)"
        ageLabel.text = "Age: \(user.dob.age)"
        emailTextView.text = "Email: \(user.email)"
    }

}
