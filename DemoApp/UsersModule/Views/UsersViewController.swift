//
//  UsersViewController.swift
//  DemoApp
//
//  Created by Nemanja Djurisic on 10/21/19.
//  Copyright © 2019 Small PDF. All rights reserved.
//

import UIKit

/// User Module View Protocol
protocol UserViewProtocol: class {
    var presenter: UserPresenterProtocol? { get set }
    var interactor: UserInteractorProtocol? { get set }
    
    // Update UI with value returned.
    /// Set the view Object of Type [User]
    func set(users: [User])
    /// Show error
    func showError(errorMsg: String)
}

class UsersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: UserPresenterProtocol?
    var interactor: UserInteractorProtocol?
    
    private var dataSource: [User] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    private var page: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildModule()
        
        interactor?.fetchUsers(forPage: page + 1)
    }
    
    func buildModule() {
        
        //MARK: Initialise components.
        let presenter = UserPresenter()
        let interactor = UserInteractor(withApiManager: UserAPIManager())
        
        //MARK: link VIP components.
        self.presenter = presenter
        self.interactor = interactor
        presenter.view = self
        interactor.presenter = presenter
    }

}

extension UsersViewController: UserViewProtocol {
    func set(users: [User]) {
        DispatchQueue.main.async {
            self.page += 1
            self.dataSource = self.dataSource + users
        }
    }
    
    func showError(errorMsg: String) {
        DispatchQueue.main.async {
            self.showAlert(title: "Error", message: errorMsg)
        }
    }
}

extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = dataSource[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.cid, for: indexPath) as! UserTableViewCell
        
        cell.updateUI(user: user)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        navigationController?.pushViewController(UserDetailViewController.instantiate(withUser: dataSource[indexPath.row]), animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == dataSource.count - 1, dataSource.count % 20 == 0  {
            interactor?.fetchUsers(forPage: page + 1)
        }
    }
    
}
